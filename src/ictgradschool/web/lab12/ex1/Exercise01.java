package ictgradschool.web.lab12.ex1;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;


public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");
            // A statement that returns some results
            while (true) {
                try (PreparedStatement stmt = conn.prepareStatement("SELECT body FROM simpledao_articles WHERE title LIKE ?")) {
                    System.out.println("Please entry the article title.");
                    String userInput = Keyboard.readInput();
                    stmt.setString(1, "%" + userInput + "%");

                    try (ResultSet r = stmt.executeQuery()) {

                        while (r.next()) {

                            String body = r.getString("body");
                            System.out.println(body);
                            System.out.println();

                        }
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
