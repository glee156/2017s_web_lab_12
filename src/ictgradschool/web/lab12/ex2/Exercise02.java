package ictgradschool.web.lab12.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;


public class Exercise02 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */

            try {
                ArticleDAO adao = new ArticleDAO();

                // A statement that returns some results
                //calling dao method for getting body
                while (true) {

                    System.out.println("Please entry the article title.");
                    String userInput = Keyboard.readInput();
                    List<String> r = adao.getArticlesByTitle(userInput);

                    for(String a : r){
                        System.out.println(a);
                        System.out.println();
                    }

                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
    }
}
