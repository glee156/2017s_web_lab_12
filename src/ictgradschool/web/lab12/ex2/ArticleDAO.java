package ictgradschool.web.lab12.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class ArticleDAO implements AutoCloseable {

   /* private final Database db;*/
    private final Connection conn;
    Properties dbProps = new Properties();
    /**
     * Creates a new ArticleDAO and establishes a connection to the given database.
     */
    public ArticleDAO() throws IOException, SQLException {

        dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("mysql.properties")) {
            dbProps.load(fis);
        }

        conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps);
        System.out.println("Connection made");
    }


    /**
     * Executes a query to get the lecturer with the given id from the db, then converts the result
     * to a Lecturer object and returns it. Returns null if the lecturer couldn't be found.
     */
    public List<String> getArticlesByTitle(String title) throws SQLException {

        ArrayList<String> articlesBody = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT body FROM simpledao_articles WHERE title LIKE ?")) {

            stmt.setString(1, "%" + title + "%");

            try (ResultSet rs = stmt.executeQuery()) {

                while (rs.next()) {
                    //String articleBody = rs.getString(1);
                   // System.out.println("body " + articleBody);
                    articlesBody.add(rs.getString("body"));

                }
            }
            return articlesBody;
        }

    }


    /**
     * Translates the current row of the given ResultSet into a Article object.
     */
    private Article articleFromResultSet(ResultSet rs) throws SQLException {
        return new Article(rs.getInt("artid"), rs.getString(2),
                rs.getString(3));
    }

    /**
     * Closes the connection to the database. Implements {@link AutoCloseable} to support try-with-resources.
     */
    @Override
    public void close() throws SQLException {
        this.conn.close();
    }
}
