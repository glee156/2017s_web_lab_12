package ictgradschool.web.lab12.ex2;

/**
 * Created by glee156 on 4/01/2018.
 */
public class Article {
    private Integer articleId;
    private String title;
    private String body;

    public Article(Integer articleId, String title, String body) {
        this.articleId = articleId;
        this.title = title;
        this.body = body;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
