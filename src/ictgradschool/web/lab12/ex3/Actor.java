package ictgradschool.web.lab12.ex3;

import ictgradschool.web.lab12.ex3.generated.tables.PfilmsActor;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import static ictgradschool.web.lab12.ex3.generated.Tables.*;

/**
 * Created by glee156 on 4/01/2018.
 */
public class Actor {
    public Actor() {

    }

    public int getUserInput() {
        System.out.println("Please enter the name of the actor you wish to get information about, or press enter to return to the previous menu");
        String userInput = Keyboard.readInput();
//        return userInput;
        try {
            if(userInput.isEmpty()) {
                return 4;
            }
            getData(userInput);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void getData(String userInput) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("mysql.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // Access the JOOQ library through this variable.
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            // Get actor id
            Result<Record> actorsId = create.select().from(PFILMS_ACTOR).where(PFILMS_ACTOR.ACTOR_FNAME.eq(userInput).or(PFILMS_ACTOR.ACTOR_LNAME.eq(userInput))).fetch();


            if(actorsId.isEmpty()){
                System.out.println("Sorry we couldn't find any actor by that name");
                int check = getUserInput();
                if (check == 4){
                   return;
                }

            }


            for(Record r : actorsId){
                Integer id = r.getValue(PFILMS_ACTOR.ACTOR_ID);
                String fullName = r.getValue(PFILMS_ACTOR.ACTOR_FNAME) + " " + r.getValue(PFILMS_ACTOR.ACTOR_LNAME);
                System.out.println(fullName + " is listed as being involved in the following films");

                Result<Record2<String, String>> filmTitle = create.select(PFILMS_FILM.FILM_TITLE, PFILMS_ROLE.ROLE_NAME).from(PFILMS_FILM, PFILMS_PARTICIPATES_IN, PFILMS_ROLE).where(PFILMS_PARTICIPATES_IN.ACTOR_ID.eq(id).and(PFILMS_FILM.FILM_ID.eq(PFILMS_PARTICIPATES_IN.FILM_ID)).and(PFILMS_PARTICIPATES_IN.ROLE_ID.eq(PFILMS_ROLE.ROLE_ID))).fetch();

                for(Record record : filmTitle){
                    String film = record.getValue(PFILMS_FILM.FILM_TITLE);
                    String role = record.getValue(PFILMS_ROLE.ROLE_NAME);
                    System.out.println(film + " (" + role + ")");
                }
            }

        }
    }
}
