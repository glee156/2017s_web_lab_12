package ictgradschool.web.lab12.ex3;

import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static ictgradschool.web.lab12.ex3.generated.Tables.*;

/**
 * Created by glee156 on 4/01/2018.
 */
public class Movie {
    public Movie() {

    }

    public int getUserInput() {
        System.out.println("Please enter the name of the actor you wish to get information about, or press enter to return to the previous menu");
        String userInput = Keyboard.readInput();
//        return userInput;
        try {
            if (userInput.isEmpty()) {
                return 4;
            }
            getData(userInput);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void getData(String userInput) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("mysql.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // Access the JOOQ library through this variable.
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            // Get film id
            Result<Record> filmsId = create.select().from(PFILMS_FILM).where(PFILMS_FILM.FILM_TITLE.eq(userInput)).fetch();

            if (filmsId.isEmpty()) {
                System.out.println("Sorry we couldn't find any actor by that name");
                int check = getUserInput();
                if (check == 4) {
                    return;
                }
            }

            for(Record r : filmsId){
                Integer id = r.getValue(PFILMS_FILM.FILM_ID);
                String filmTitle = r.getValue(PFILMS_FILM.FILM_TITLE);
                String filmGenre = r.getValue(PFILMS_FILM.GENRE_NAME);
                String article;
                if(filmGenre.charAt(0) == 'a'){
                    article = "an";
                }
                else{
                    article = "a";
                }

                System.out.println(filmTitle + " is " + article + " " + filmGenre + " film that features the following people");

                /*Result<Record3<String, String, String>> actorsName = create.select(PFILMS_ACTOR.ACTOR_FNAME, PFILMS_ACTOR.ACTOR_LNAME, PFILMS_ROLE.ROLE_NAME).from(PFILMS_FILM, PFILMS_PARTICIPATES_IN, PFILMS_ROLE, PFILMS_ACTOR).where(PFILMS_PARTICIPATES_IN.ACTOR_ID.eq(PFILMS_ACTOR.ACTOR_ID).and(PFILMS_FILM.FILM_ID.eq(PFILMS_PARTICIPATES_IN.FILM_ID)).and(PFILMS_PARTICIPATES_IN.ROLE_ID.eq(PFILMS_ROLE.ROLE_ID))).fetch();*/

                //not sure how to connect PFILMS_ACTOR and PFILMS_ROLE databases...
           Result<Record3<String, String, String>> actorsName = create.select(PFILMS_ACTOR.ACTOR_FNAME, PFILMS_ACTOR.ACTOR_LNAME, PFILMS_ROLE.ROLE_NAME).from(PFILMS_ACTOR, PFILMS_ROLE, PFILMS_PARTICIPATES_IN).where(PFILMS_ACTOR.ACTOR_ID.eq(PFILMS_PARTICIPATES_IN.ACTOR_ID).and(PFILMS_PARTICIPATES_IN.ROLE_ID.eq(PFILMS_ROLE.ROLE_ID)).and(PFILMS_PARTICIPATES_IN.FILM_ID.eq(id))).fetch();

                for(Record record : actorsName){
                    String actorFirstName = record.getValue(PFILMS_ACTOR.ACTOR_FNAME);
                    String actorLastName = record.getValue(PFILMS_ACTOR.ACTOR_LNAME);
                    String role = record.getValue(PFILMS_ROLE.ROLE_NAME);
                    System.out.println(actorFirstName + " " + actorLastName + " (" + role + ")");
                }
            }

        }
    }
}