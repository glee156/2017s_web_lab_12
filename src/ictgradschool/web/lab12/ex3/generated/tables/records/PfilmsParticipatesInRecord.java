/*
 * This file is generated by jOOQ.
*/
package ictgradschool.web.lab12.ex3.generated.tables.records;


import ictgradschool.web.lab12.ex3.generated.tables.PfilmsParticipatesIn;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.2"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class PfilmsParticipatesInRecord extends UpdatableRecordImpl<PfilmsParticipatesInRecord> implements Record3<Integer, Integer, Integer> {

    private static final long serialVersionUID = -982720603;

    /**
     * Setter for <code>glee156.pfilms_participates_in.actor_id</code>.
     */
    public void setActorId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>glee156.pfilms_participates_in.actor_id</code>.
     */
    public Integer getActorId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>glee156.pfilms_participates_in.film_id</code>.
     */
    public void setFilmId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>glee156.pfilms_participates_in.film_id</code>.
     */
    public Integer getFilmId() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>glee156.pfilms_participates_in.role_id</code>.
     */
    public void setRoleId(Integer value) {
        set(2, value);
    }

    /**
     * Getter for <code>glee156.pfilms_participates_in.role_id</code>.
     */
    public Integer getRoleId() {
        return (Integer) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record2<Integer, Integer> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, Integer, Integer> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<Integer, Integer, Integer> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return PfilmsParticipatesIn.PFILMS_PARTICIPATES_IN.ACTOR_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return PfilmsParticipatesIn.PFILMS_PARTICIPATES_IN.FILM_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return PfilmsParticipatesIn.PFILMS_PARTICIPATES_IN.ROLE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getActorId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component2() {
        return getFilmId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component3() {
        return getRoleId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getActorId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getFilmId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getRoleId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PfilmsParticipatesInRecord value1(Integer value) {
        setActorId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PfilmsParticipatesInRecord value2(Integer value) {
        setFilmId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PfilmsParticipatesInRecord value3(Integer value) {
        setRoleId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PfilmsParticipatesInRecord values(Integer value1, Integer value2, Integer value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached PfilmsParticipatesInRecord
     */
    public PfilmsParticipatesInRecord() {
        super(PfilmsParticipatesIn.PFILMS_PARTICIPATES_IN);
    }

    /**
     * Create a detached, initialised PfilmsParticipatesInRecord
     */
    public PfilmsParticipatesInRecord(Integer actorId, Integer filmId, Integer roleId) {
        super(PfilmsParticipatesIn.PFILMS_PARTICIPATES_IN);

        set(0, actorId);
        set(1, filmId);
        set(2, roleId);
    }
}
