package ictgradschool.web.lab12.ex3;

import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static ictgradschool.web.lab12.ex3.generated.Tables.*;

/**
 * Created by glee156 on 4/01/2018.
 */
public class Genre {
    public Genre() {

    }

    public int getUserInput() {
        System.out.println("Please enter the name of the genre you wish to get information about, or press enter to return to the previous menu");
        String userInput = Keyboard.readInput();
//        return userInput;
        try {
            if (userInput.isEmpty()) {
                return 4;
            }
            getData(userInput);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void getData(String userInput) throws SQLException, IOException {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("mysql.properties")) {
            dbProps.load(fis);
        }

        // Establishing connection to the database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            // Access the JOOQ library through this variable.
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            // Get film id
            Result<Record> genreNames = create.select().from(PFILMS_GENRE).where(PFILMS_GENRE.GENRE_NAME.eq(userInput)).fetch();

            if (genreNames.isEmpty()) {
                System.out.println("Sorry we couldn't find any genre by that name");
                int check = getUserInput();
                if (check == 4) {
                    return;
                }
            }

            for(Record r : genreNames){
                String currentGenreName = r.getValue(PFILMS_GENRE.GENRE_NAME);

                System.out.println("The genre " + currentGenreName + " includes the following films");

                Result<Record1<String>> filmsName = create.select(PFILMS_FILM.FILM_TITLE).from(PFILMS_FILM, PFILMS_GENRE).where(PFILMS_FILM.GENRE_NAME.eq(PFILMS_GENRE.GENRE_NAME).and(PFILMS_GENRE.GENRE_NAME.eq(currentGenreName))).fetch();

                for(Record record : filmsName){
                    String currentFilmName = record.getValue(PFILMS_FILM.FILM_TITLE);
                    System.out.println(currentFilmName);
                }
            }

        }
    }
}