package ictgradschool.web.lab12.ex3;

/**
 * Created by glee156 on 4/01/2018.
 */
public class FilmDatabaseRunner {
    public static void main(String[] args) {
        FilmDatabaseRunner runner = new FilmDatabaseRunner();
        runner.start();
    }

    public void start(){
        System.out.println("Welcome to the Film database!");

        //ask user for choice
        Boolean breakOut = true;
        while(breakOut) {
            String userChoice = getUserChoice();

            if (userChoice.equals("1")) {
                Actor current = new Actor();
                int check = current.getUserInput();
                if (check == 4) {
                    breakOut = false;
                }
                if(check == 0){
                    current.getUserInput();
                }
            } else if (userChoice.equals("2")) {
                Movie current = new Movie();
                int check = current.getUserInput();
                if (check == 4) {
                    breakOut = false;
                }
                if(check == 0){
                    current.getUserInput();
                }

            } else if (userChoice.equals("3")) {
                Genre current = new Genre();
                int check = current.getUserInput();
                if (check == 4) {
                    breakOut = false;
                }
                if(check == 0){
                    current.getUserInput();
                }

            } else {
                return;
            }
        }

    }

    public String getUserChoice(){
        //ask user for choice
        System.out.println("Please select an option from the following \n1. Information by Actor \n2. information by Movie \n3. Information by Genre \n4. Exit");
        System.out.print("> ");
        String userChoice = Keyboard.readInput();
        return userChoice;
    }
}
